import torch
import numpy as np
import torch.nn as nn
# from .. import DEVICE # Imagine you have a GPU device and write code using it.
device=torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

class FallDetecion():
    def __init__(self):
        pass
        #self.N = N
        #self.yolo = YOLO('yolov8n-pose.pt')
        #self.net = SimpleNN(self.N)
        #self.nnet.load_state_dict(self.yolo.state_dict())
        #self.nnet.eval()

    def __call__(self, skeleton_cache):
        '''
            This __call__ function takes a cache of skeletons as input, with a shape of (M x 17 x 2), where M represents the number of skeletons.
            The value of M is constant and represents time. For example, if you have a 7 fps stream and M is equal to 7 (M = 7), it means that the cache length is 1 second.
            The number 17 represents the count of points in each skeleton (as shown in skeleton.png), and 2 represents the (x, y) coordinates.

            This function uses the cache to detect falls.

            The function will return:
                - bool: isFall (True or False)
                - float: fallScore
        '''

        diff=self.sign1(skeleton_cache)
        diff=self.simple_moving_average(diff)
        mean_derivative_value=torch.mean(diff)
        print(mean_derivative_value)
        threshold=10

        isFall = mean_derivative_value > threshold
        fallScore=mean_derivative_value/(threshold/1.5+mean_derivative_value)

        angles=self.sign2(skeleton_cache)
        angles=self.simple_moving_average(angles)
        mean_angle_value=torch.mean(angles)
        if mean_angle_value>100:
            mean_angle_value=abs(180-mean_angle_value)
        if isFall==True and mean_angle_value>77:
            isFall = False
            fallScore = (torch.rand(1, dtype=torch.float32) * 0.1 + 0.3).item()
        if isFall==False and mean_angle_value<45:
            isFall=True
            fallScore=(torch.rand(1, dtype=torch.float32)*0.1 + 0.8).item()
        return isFall, fallScore
    

    
    def sign1(self, skeleton_cache):
        pnt = (skeleton_cache[0][11] + skeleton_cache[0][12]) / 2
        pnt[1] = pnt[1] + torch.norm(skeleton_cache[0][6] - skeleton_cache[0][12]) / 4

        vector = torch.zeros(skeleton_cache.shape[0] - 1)
        for i in range(skeleton_cache.shape[0] - 1):
            # Derivative of the coordinate change of the point 13 which is
            pnt_next = (skeleton_cache[i+1][11] + skeleton_cache[i+1][12]) / 2
            pnt_next[1] = pnt_next[1] + torch.norm(skeleton_cache[i+1][6] - skeleton_cache[i+1][12]) / 4

            #First calculate the diff of y,and then in the call function *fps
            diff = torch.abs(pnt_next[1]-pnt[1])

            # Store the result in the vector
            vector[i] = diff

        return vector

    horizontal=np.array([100,0])

    def calculate_angles_between_two_vectors(self, vec1, vec2): # vec1 and vec2 are numpy arrays
        cos_angle = np.dot(vec1, vec2) / (np.linalg.norm(vec1) * np.linalg.norm(vec2))
        return np.arccos(cos_angle) * 180 / np.pi

    def sign2(self, skeleton_cache):
        angles=torch.zeros(skeleton_cache.shape[0] - 1)
        for i in range(skeleton_cache.shape[0] - 1):
            neck_center=(skeleton_cache[i][5] + skeleton_cache[i][6]) / 2
            hip_center=(skeleton_cache[i][11] + skeleton_cache[i][12]) / 2

            centerline=(neck_center-hip_center).numpy()
            angles[i]= self.calculate_angles_between_two_vectors(centerline, self.horizontal)

        return angles

    def simple_moving_average(self, input_tensor):
        moving_avg_tensor = torch.zeros_like(input_tensor)
        cumulative_sum = 0.0

        for i in range(len(input_tensor)):
            cumulative_sum += input_tensor[i]
            moving_avg_tensor[i] = cumulative_sum / (i + 1)

        return moving_avg_tensor
